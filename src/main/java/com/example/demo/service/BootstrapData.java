package com.example.demo.service;

import com.example.demo.model.Card;
import com.example.demo.model.CardInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Transactional
@Component
public class BootstrapData implements CommandLineRunner {

    @Autowired
    public CardRepo cardRepo;

    @Override
    public void run(String... args) throws Exception {
        Card card = Card.builder().cardNumber("123").description("some card").build();
        CardInfo cardInfo = CardInfo.builder().cardNumber("123").info("some info").build();
        card.setCardInfo(cardInfo);

        cardRepo.save(card);
        System.out.println("Card info: " + card.getCardInfo().getInfo());
    }
}
